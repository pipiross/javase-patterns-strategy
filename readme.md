# Description

Calculate discount for the order according to following rules:

* If the order includes three or more items, then discount is 40%.
* If the order includes two items, then discount is 25%.
* If a total amount of the order is greater than $40, then discount is $5.
* Otherwise, discount is $0.