package hrytsenko;

import java.math.BigDecimal;
import java.util.List;

public interface Discount {

    boolean isApplicable(List<Item> items);

    BigDecimal calculate(List<Item> items);

    default BigDecimal total(List<Item> items) {
        return items.stream().map(item -> item.getSubtotal()).reduce(BigDecimal.ZERO,
                (total, subtotal) -> total.add(subtotal));
    }

}
