package hrytsenko.discount;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.base.Preconditions;

import hrytsenko.Discount;
import hrytsenko.Item;

public class FixedDiscountForTotalAmount implements Discount {

    private BigDecimal amount;
    private BigDecimal discount;

    public FixedDiscountForTotalAmount(BigDecimal discount, BigDecimal amount) {
        Preconditions.checkArgument(amount.compareTo(BigDecimal.ZERO) > 0);
        Preconditions.checkArgument(amount.compareTo(discount) > 0);

        this.amount = amount;
        this.discount = discount;
    }

    @Override
    public boolean isApplicable(List<Item> items) {
        return total(items).compareTo(amount) >= 0;
    }

    @Override
    public BigDecimal calculate(List<Item> items) {
        return discount;
    }

}
