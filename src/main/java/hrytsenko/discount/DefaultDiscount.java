package hrytsenko.discount;

import java.math.BigDecimal;
import java.util.List;

import hrytsenko.Discount;
import hrytsenko.Item;

public class DefaultDiscount implements Discount {

    @Override
    public boolean isApplicable(List<Item> items) {
        return true;
    }

    @Override
    public BigDecimal calculate(List<Item> items) {
        return BigDecimal.ZERO;
    }

}
