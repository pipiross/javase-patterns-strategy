package hrytsenko.discount;

import java.math.BigDecimal;
import java.util.List;

import com.google.common.base.Preconditions;

import hrytsenko.Discount;
import hrytsenko.Item;

public class PercentDiscountForSeveralItems implements Discount {

    private int itemsNum;
    private BigDecimal discount;

    public PercentDiscountForSeveralItems(BigDecimal discount, int itemsNum) {
        Preconditions.checkArgument(discount.compareTo(BigDecimal.ONE) < 0);
        Preconditions.checkArgument(itemsNum > 0);

        this.itemsNum = itemsNum;
        this.discount = discount;
    }

    @Override
    public boolean isApplicable(List<Item> items) {
        return items.size() >= itemsNum;
    }

    @Override
    public BigDecimal calculate(List<Item> items) {
        return total(items).multiply(discount);
    }

}
