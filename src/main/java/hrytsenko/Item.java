package hrytsenko;

import java.math.BigDecimal;

public class Item {

    private final String title;
    private final BigDecimal subtotal;

    public Item(String title, BigDecimal subtotal) {
        this.title = title;
        this.subtotal = subtotal;
    }

    public String getTitle() {
        return title;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

}
