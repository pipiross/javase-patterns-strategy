package hrytsenko;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import hrytsenko.discount.DefaultDiscount;

public class DiscountService {

    private List<Discount> discounts;

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    public BigDecimal calculateDiscount(List<Item> items) {
        return discounts.stream().filter(discount -> discount.isApplicable(items)).findFirst()
                .orElse(new DefaultDiscount()).calculate(items).setScale(2, RoundingMode.HALF_UP);
    }

}
