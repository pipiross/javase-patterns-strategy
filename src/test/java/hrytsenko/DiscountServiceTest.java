package hrytsenko;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import hrytsenko.discount.FixedDiscountForTotalAmount;
import hrytsenko.discount.PercentDiscountForSeveralItems;

public class DiscountServiceTest {

    private DiscountService discountService;

    @Before
    public void init() {
        discountService = new DiscountService();
        discountService.setDiscounts(Arrays.asList(new PercentDiscountForSeveralItems(discount(0.4), 3),
                new PercentDiscountForSeveralItems(discount(0.25), 2),
                new FixedDiscountForTotalAmount(discount(5.00), amount(40.00))));
    }

    @Test
    public void threeItems_percentDiscount40() {
        BigDecimal discount = discountService.calculateDiscount(items(new Item("Java 8 In Action", amount(39.99)),
                new Item("Groovy In Action", amount(47.99)), new Item("Scala In Action", amount(35.99))));

        Assert.assertEquals(amount(49.59), discount);
    }

    @Test
    public void twoItems_percentDiscount25() {
        BigDecimal discount = discountService.calculateDiscount(
                items(new Item("Java 8 In Action", amount(39.99)), new Item("Groovy In Action", amount(47.99))));

        Assert.assertEquals(amount(22.00), discount);
    }

    @Test
    public void oneItem_fixedDiscount() {
        BigDecimal discount = discountService.calculateDiscount(items(new Item("Groovy In Action", amount(47.99))));

        Assert.assertEquals(amount(5.00), discount);
    }

    @Test
    public void oneItem_noDiscount() {
        BigDecimal discount = discountService.calculateDiscount(items(new Item("Scala In Action", amount(35.99))));

        Assert.assertEquals(amount(0.00), discount);
    }

    private static BigDecimal discount(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP);
    }

    private static BigDecimal amount(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP);
    }

    private static List<Item> items(Item... items) {
        return Arrays.asList(items);
    }

}
